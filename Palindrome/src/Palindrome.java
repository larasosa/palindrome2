
public class Palindrome {



		public static boolean isPalindrome(String input) {
			
			input = input.toLowerCase().replaceAll(" ", "");
			
			for (int i = 0, j= input.length() -1; 1<j; i++, j--) {
				if (input.charAt(i) != input.charAt (j)) {
					return false;
				}
			}
			
			
			return true;
		}
		public static void main(String[] args) {
			System.out.println("is anna a Palindrome ?" + Palindrome.isPalindrome("anna"));
			System.out.println("is anna a Palindrome ?" + Palindrome.isPalindrome("race car"));

			System.out.println("is anna a Palindrome ?" + Palindrome.isPalindrome("taco cat"));

			System.out.println("is anna a Palindrome ?" + Palindrome.isPalindrome("not a palindrome"));

			
		}

	}


